/**
 * CSE110: Team 25
 * PlaceIt class
 * Holds information pertaining to a single Place-it
 */
package com.CSE110.Team25.placeitapp;

import java.util.ArrayList;

/**
 *
 * @author Daniel
 * Class that contains the data of place-its to store in the database. 
 */
public class PlaceIt {
    private int id;
    private String title;
    private String desc;
    private double latitude;
    private double longitude;
    private String create_time;
    private boolean due_flag;
    private String rep_date;
    private String cat1;
    private String cat2;
    private String cat3;
    private boolean isPlaceit;

    /**
     * Constructor
     */
    public PlaceIt(String title, String desc, double langitude, double longitude, String create_time,
                   boolean due_flag, String rep_date, String cat1, String cat2, String cat3, boolean isPlaceit)
    {
        this.title = title;
        this.desc = desc;
        this.latitude = langitude;
        this.longitude = longitude;
        this.create_time = create_time;
        this.due_flag = due_flag;
        this.rep_date = rep_date;
        this.cat1 = cat1;
        this.cat2 = cat2;
        this.cat3 = cat3;
        this.isPlaceit = isPlaceit;
    }

    /**
     * Default Constructor
     */
    public PlaceIt() {}

    /**
     * Getter and setters for all instance variables
     */
    public int getId() {  //Id
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {	//Title
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    
    public String getDesc() {  //Description
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public double getLatitude() {  //Latitude
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {  //Longitude
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getCreate_time() {  //Created Time
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public boolean isDue_flag() {  //The due time
        return due_flag;
    }

    public void setDue_flag(boolean due_flag) {
        this.due_flag = due_flag;
    }

    public String getRep_date() {  //Repeating Date
        return rep_date;
    }

    public void setRep_date(String rep_date) {
        this.rep_date = rep_date;
    }


    public boolean isPlaceit() {  //Check for place-it
        return isPlaceit;
    }

    public void setIsPlaceit(boolean isPlaceit) {
        this.isPlaceit = isPlaceit;
    }

    public String getCat1() {  //Category 1
        return cat1;
    }

    public void setCat1(String cat1) {
        this.cat1 = cat1;
    }

    public String getCat2() {  //Category 2
        return cat2;
    }

    public void setCat2(String cat2) {
        this.cat2 = cat2;
    }

    public String getCat3() {  //Category 3
        return cat3;
    }

    public void setCat3(String cat3) {
        this.cat3 = cat3;
    }

    /**
     * Array for all three categories
     * @return Array storing categories
     */
    public ArrayList<String> getCategories(){
        ArrayList<String> res = new ArrayList<String>();
        if(!this.cat1.equals("None")) res.add(this.cat1);
        if(!this.cat2.equals("None")) res.add(this.cat2);
        if(!this.cat3.equals("None")) res.add(this.cat3);
        return res;
    }

    @Override
    public String toString() {
        return "Title: " + title + ", Desc: " + desc + ", Lat: "+ latitude +
                ", Lon: "+ longitude + ", isPI: " + isPlaceit;
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof PlaceIt) && (((PlaceIt) o).getLatitude() == this.latitude
                && ((PlaceIt) o).getLongitude() == this.longitude);
    }
}
