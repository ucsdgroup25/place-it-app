package com.CSE110.Team25.placeitapp;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by Kenny on 2/18/14.
 * Class that list all the place-it in the map. 
 * It allows the user to see place-its on the map as a list and to see the details information
 * by clicking on place-it
 */
public class PlaceItListFragment extends ListFragment {
    OnPlaceItSelectedListener mCallback;
    PlaceitHandler piHandler;
    ArrayList<PlaceIt> pi_list;

    // Container Activity must implement this interface
    public interface OnPlaceItSelectedListener {
        public void onPlaceItSelected(PlaceIt placeIt);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        piHandler = PlaceitHandler.getHelper(getActivity());
        pi_list = piHandler.getAllPlaceIts();
        ArrayAdapter<PlaceIt> adapter = new ArrayAdapter<PlaceIt>(getActivity(), R.layout.placeit_list, pi_list);
        setListAdapter(adapter);
    }

    /**
     * 
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnPlaceItSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnPlaceItSelectedListener");
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        // Send the event to the host activity
        mCallback.onPlaceItSelected(pi_list.get(position));
    }

}

