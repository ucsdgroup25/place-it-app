/**
 * CSE110: Team 25
 * PlaceItHandler class
 * Maintains the database connection throughout the lifecycle
 * of the app and allows modifications to the database
 */
package com.CSE110.Team25.placeitapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.util.Log;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * @author "Ken Yokoyama"
 */
public class PlaceitHandler {
    // Database fields
    private static PlaceitHandler instance;
    private SQLiteDatabase database;
    private MySQLHelper dbHelper;
    private String[] allColumns = { MySQLHelper.KEY_ROWID,
            MySQLHelper.KEY_TITLE,
            MySQLHelper.KEY_DESC,
            MySQLHelper.KEY_LANG,
            MySQLHelper.KEY_LONG,
            MySQLHelper.KEY_CREATE,
            MySQLHelper.KEY_DUE_FLAG,
            MySQLHelper.KEY_REPEAT_DAY,
            MySQLHelper.KEY_CAT1,
            MySQLHelper.KEY_CAT2,
            MySQLHelper.KEY_CAT3,
            MySQLHelper.KEY_IS_PI };
    private double RADIUS = 1.5/0.00062137;


    /**
     * Constructor
     * @param context
     */
    public PlaceitHandler(Context context) {
        dbHelper = new MySQLHelper(context);
    }

    /**
     * Returns the PlaceItHandler instance. Makes sure that there is always only
     * one instance of the class running at a time.
     * @param context
     * @return PlaceItHandler instance
     */
    public static synchronized PlaceitHandler getHelper(Context context){
        if( instance == null ){
            instance = new PlaceitHandler(context);
        }
        return instance;
    }

    /**
     * Open the SQLite database for writing
     * @throws android.database.SQLException
     */
    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    /**
     * Closes the SQLite database
     */
    public void close() {
        dbHelper.close();
    }

    /**
     * Adds the place-it to the database
     * @param pi - PlaceIt object to be saved
     */
    public void addPlaceIt(PlaceIt pi) {
        ContentValues values = new ContentValues();
        values.put(MySQLHelper.KEY_TITLE, pi.getTitle());
        values.put(MySQLHelper.KEY_DESC, pi.getDesc());
        values.put(MySQLHelper.KEY_LANG, Double.toString(pi.getLatitude()));
        values.put(MySQLHelper.KEY_LONG, Double.toString(pi.getLongitude()));
        values.put(MySQLHelper.KEY_CREATE, pi.getCreate_time());
        values.put(MySQLHelper.KEY_DUE_FLAG, Boolean.toString(pi.isDue_flag()));
        values.put(MySQLHelper.KEY_REPEAT_DAY, pi.getRep_date());
        values.put(MySQLHelper.KEY_CAT1, pi.getCat1());
        values.put(MySQLHelper.KEY_CAT2, pi.getCat2());
        values.put(MySQLHelper.KEY_CAT3, pi.getCat3());
        values.put(MySQLHelper.KEY_IS_PI, Boolean.toString(pi.isPlaceit()));
        database.insert(MySQLHelper.DATABASE_TABLE, null, values);
    }

    /**
     * Deletes all Place-it entries from the database
     */
    public void deleteAllPlaceIt() {
        database.delete(MySQLHelper.DATABASE_TABLE, null, null);
    }

    /**
     * Deletes a Place-it from the database
     * @param placeit - Place-it object
     */
    private void deletePlaceIt(PlaceIt placeit) {
        long id = placeit.getId();
        System.out.println("PlaceIt deleted with id: " + id);
        Log.d("deletePlaceIt", "PlaceIt: "+placeit.getTitle()+" id: "+placeit.getId()+" DELETED");
        database.delete(MySQLHelper.DATABASE_TABLE, MySQLHelper.KEY_ROWID + " = " + id, null);
    }

    /**
     * Deletes a Place-it from a specific latitude and longitude
     * @param lat - latitude value
     * @param lon - longitude value
     */
    public void deletePlaceItAt(double lat, double lon){
        PlaceIt target = null;
        Cursor cursor = database.query(MySQLHelper.DATABASE_TABLE,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PlaceIt placeit = cursorToPlaceIt(cursor);
            if(placeit.getLatitude() == lat && placeit.getLongitude() == lon)
                target = placeit;
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        if(target != null){
            //Log.d("deletePlaceitAt", "Processing deletion..."+target.getTitle());
            deletePlaceIt(target);
        }
    }

    /**
     * Fetches the Place-it at a specific location from the database
     * @param lat - latitude of the place-it
     * @param lon - longitude of the place-it
     * @return the specified Place-it class
     */
    public PlaceIt getPlaceItAt(double lat, double lon){
        PlaceIt target = null;
        Cursor cursor = database.query(MySQLHelper.DATABASE_TABLE,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PlaceIt placeit = cursorToPlaceIt(cursor);
            if(placeit.getLatitude() == lat && placeit.getLongitude() == lon)
                target = placeit;
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return target;
    }
    /**
     * Get array of all entries and add the place-it
     * @return place-it array
     */
    public ArrayList<PlaceIt> getAllEntries() {
        ArrayList<PlaceIt> placeits = new ArrayList<PlaceIt>();

        Cursor cursor = database.query(MySQLHelper.DATABASE_TABLE,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PlaceIt placeit = cursorToPlaceIt(cursor);
            placeits.add(placeit);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return placeits;
    }

    /**
     * Return all the Place-its in the database.
     * Includes expired Place-its too at the moment
     * @return List of all Place-its
     */
    public ArrayList<PlaceIt> getAllPlaceIts() {
        ArrayList<PlaceIt> placeits = new ArrayList<PlaceIt>();

        Cursor cursor = database.query(MySQLHelper.DATABASE_TABLE,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PlaceIt placeit = cursorToPlaceIt(cursor);
            if(placeit.isPlaceit()){
                placeits.add(placeit);
            }
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return placeits;
    }

    /**
     * Used to get all discarded place-it 
     * @return Array of place-its that is not active
     */
    public ArrayList<PlaceIt> getAllNonPlaceIts() {
        ArrayList<PlaceIt> placeits = new ArrayList<PlaceIt>();

        Cursor cursor = database.query(MySQLHelper.DATABASE_TABLE,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PlaceIt placeit = cursorToPlaceIt(cursor);
            if(!placeit.isPlaceit()){
                placeits.add(placeit);
            }
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return placeits;
    }

    /**
     * Returns a list of Place-its in the 1.5 mile radius of a specified location
     * @param lat - latitude of the current location
     * @param lon - longitude of the current location
     * @return List of place-its in the 1.5 mile radius
     */
    public List<PlaceIt> getAllPlaceItsAround(double lat, double lon){
        List<PlaceIt> placeits = new ArrayList<PlaceIt>();

        Cursor cursor = database.query(MySQLHelper.DATABASE_TABLE,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PlaceIt placeit = cursorToPlaceIt(cursor);
            if(placeit.isPlaceit()){
                // Check if place it is in 1.5 mile radius
                float[] distance = new float[2];
                Location.distanceBetween(lat, lon, placeit.getLatitude(), placeit.getLongitude(), distance);
                if(distance[0] < RADIUS && distance[1] < RADIUS && placeit.isDue_flag())
                    placeits.add(placeit);
            }
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return placeits;
    }

    /**
     * Returns a list of non-active place-its in the 1.5 mile radius of a specified location
     * @param lat - latitude of the current location
     * @param lon - longitude of the current location
     * @return List of non-active place-its in the 1.5 mile radius
     */
    public List<PlaceIt> getAllNonPlaceItsAround(double lat, double lon){
        List<PlaceIt> placeits = new ArrayList<PlaceIt>();

        Cursor cursor = database.query(MySQLHelper.DATABASE_TABLE,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PlaceIt placeit = cursorToPlaceIt(cursor);
            if(!placeit.isPlaceit()){
                // Check if place it is in 1.5 mile radius
                float[] distance = new float[2];
                Location.distanceBetween(lat, lon, placeit.getLatitude(), placeit.getLongitude(), distance);
                if(distance[0] < RADIUS && distance[1] < RADIUS && placeit.isDue_flag())
                    placeits.add(placeit);
            }
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return placeits;
    }

    /**
     * Updates the place-it information in the database
     * @param piId - ID of the place-it in the database
     * @param column - The column that should have the updated values
     * @param newVal - the string value to be inserted
     */
    public void updatePlaceItInDatabase(int piId, String column, String newVal){
        ContentValues args = new ContentValues();
        args.put(column, newVal);
        database.update(MySQLHelper.DATABASE_TABLE, args, "_id "+"="+piId, null);
    }

    public String getAllUsedCategoriesAsString(){
        ArrayList<String> res = new ArrayList<String>();

        Cursor cursor = database.query(MySQLHelper.DATABASE_TABLE,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PlaceIt placeit = cursorToPlaceIt(cursor);
            if(placeit.isPlaceit()){
                for(String s: placeit.getCategories()){
                    if(!s.equals("None") && !res.contains(s)){
                        res.add(s);
                    }
                }
            }
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();

        String acc = "";
        for(String s: res){
            acc += (s + "|");
        }
        return acc.substring(0, acc.length());
    }

    /**
     * Returns array of place-its with category specified
     * @param cats - array to distinguish the categories
     * @return List of place-its with categories
     */
    public ArrayList<PlaceIt> getPlaceItsWithCategories(ArrayList<String> cats){
        ArrayList<PlaceIt> res = new ArrayList<PlaceIt>();

        Cursor cursor = database.query(MySQLHelper.DATABASE_TABLE,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PlaceIt placeit = cursorToPlaceIt(cursor);
            if(placeit.isPlaceit()){
                if(cats.contains(placeit.getCat1()) ||
                        cats.contains(placeit.getCat2()) ||
                        cats.contains(placeit.getCat3())){
                    res.add(placeit);
                }
            }
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();

        return res;
    }

    /**
     * Returns boolean checking if place-it is in the database
     * @param lat - latitude
     * @param lon - longitude
     * @return True if the place-it exist, else false
     */
    public boolean piIsInDatabase(double lat, double lon){
        Cursor cursor = database.query(MySQLHelper.DATABASE_TABLE,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PlaceIt placeit = cursorToPlaceIt(cursor);
            if((placeit.getLatitude() == lat) &&
                    (placeit.getLongitude() == lon)){
                cursor.close();
                return true;
            }
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return false;
    }

    /**
     * Restore scheduled place-its to be active
     */
    public void restoreScheduledPlaceIts(){
        Cursor cursor = database.query(MySQLHelper.DATABASE_TABLE,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PlaceIt placeit = cursorToPlaceIt(cursor);
            if(!placeit.isDue_flag() && !placeit.getRep_date().equals("none")){
                if(translateDayToInt(placeit.getRep_date()) == Calendar.DAY_OF_WEEK){
                    updatePlaceItInDatabase(placeit.getId(), MySQLHelper.KEY_DUE_FLAG, "true");
                }
            }
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
    }

    /**
     * Take the String for day and convert it to integer value
     * @param day - String indicating the which day is it
     * @return Proper integer value for specific day
     */
    private int translateDayToInt(String day){
        if(day.equals("sunday")){
            return 1;
        }else if(day.equals("monday")){
            return 2;
        }else if(day.equals("tuesday")){
            return 3;
        }else if(day.equals("wednesday")){
            return 4;
        }else if(day.equals("thursday")){
            return 5;
        }else if(day.equals("friday")){
            return 6;
        }else{
            return 7;
        }
    }

    /**
     * Converts the cursor into a place-it class from the database
     * @param cursor - cursor to the specified row on the database
     * @return a Place-it class using the data from the database
     */
    private PlaceIt cursorToPlaceIt(Cursor cursor) {
        PlaceIt placeit = new PlaceIt();
        placeit.setId(cursor.getInt(0));
        placeit.setTitle(cursor.getString(1));
        placeit.setDesc(cursor.getString(2));
        placeit.setLatitude(Double.parseDouble(cursor.getString(3)));
        placeit.setLongitude(Double.parseDouble(cursor.getString(4)));
        placeit.setCreate_time(cursor.getString(5));
        placeit.setDue_flag(Boolean.parseBoolean(cursor.getString(6)));
        placeit.setRep_date(cursor.getString(7));
        placeit.setCat1(cursor.getString(8));
        placeit.setCat2(cursor.getString(9));
        placeit.setCat3(cursor.getString(10));
        placeit.setIsPlaceit(Boolean.parseBoolean(cursor.getString(11)));

        return placeit;
    }
}
