/**
 * CSE110: Team 25
 * MyNotificationHelper class
 * (Mediator Pattern)
 * A custom notification builder class that allows us to modify
 * the behavior of the built-in notifications
 */
package com.CSE110.Team25.placeitapp;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

/**
 * @author Daniel
 */
public class MyNotificationHelper {
	
	private NotificationManager mNM;
	private NotificationCompat.Builder mBuilder;
	
	private int key;
	
	// Unique Identification Number for the Notification.
    // We use it on Notification start, and to cancel it.
    private int NOTIFICATION = R.string.local_service_started;

    // DEBUG
    // showNotification();

    /**
     * Default Constructor
     */
	MyNotificationHelper() {
		
	}

    /**
     * Constructor
     * @param context - Context
     * @param small_title - Title for the smaller notification
     * @param small_text - Description for the smaller notification
     * @param big_title - Title for the expanded notification
     * @param big_text - Description for the expanded notification
     * @param big_summary - Long description for the expanded notification
     * @param small_icon - Icon for the notification
     * @param key - The ID of the Place-it
     */
	MyNotificationHelper(Context context, String small_title, String small_text, String big_title,
                         String big_text, String big_summary, int small_icon, int key) {
		//Set up the repost and delete action buttons that will appear in the notification
		Intent repostIntent = new Intent(context, MainActivity.class);
		repostIntent.setAction("repost");
		repostIntent.putExtra("myID", key);
		//repostIntent.addCategory("android.intent.category.LAUNCHER");
		PendingIntent piRepost = PendingIntent.getActivity(context, key, repostIntent, Intent.FILL_IN_ACTION);
		
		Intent deleteIntent = new Intent(context, MainActivity.class);
		deleteIntent.setAction("discard");
		deleteIntent.putExtra("myID", key);
		//deleteIntent.addCategory("android.intent.category.LAUNCHER");
		PendingIntent piDiscard = PendingIntent.getActivity(context, key, deleteIntent, Intent.FILL_IN_ACTION);

        // Building the custom notification
		mBuilder = new NotificationCompat.Builder(context);
		mBuilder.setContentTitle(small_title);
		mBuilder.setContentText(small_text);
		mBuilder.setSmallIcon(small_icon);
		//mBuilder.setDefaults(Notification.DEFAULT_ALL);
		mBuilder.setStyle(new NotificationCompat.BigTextStyle()
			.setBigContentTitle(big_title)
			.bigText(big_text)
			.setSummaryText(big_summary));
		mBuilder.addAction(R.drawable.ic_action_refresh,
			context.getString(R.string.noti_repost), piRepost)
		.addAction (R.drawable.ic_action_discard,
			context.getString(R.string.noti_discard), piDiscard);
	
		// Creates an explicit intent for an Activity in your app
        // Intent resultIntent = new Intent(context, MainActivity.class);
	    // resultIntent.putExtra(CommonConstants.EXTRA_MESSAGE, big_title);
		// resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK || Intent.FLAG_ACTIVITY_CLEAR_TASK);
		
		Intent resultIntent = new Intent(context, MainActivity.class);
		resultIntent.setAction("notification");
		resultIntent.putExtra("myID", key);
		resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
				Intent.FLAG_ACTIVITY_CLEAR_TASK);
		//deleteIntent.addCategory("android.intent.category.LAUNCHER");

		// The stack builder object will contain an artificial back stack for the
		// started Activity.
		// This ensures that navigating backward from the Activity leads out of
		// your application to the Home screen. 
		
		/*THIS IS NOT USED ANYMORE! 
		//It is for the intent called when the placeit is pressed (not the buttons)
		 
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
		// Adds the back stack for the Intent (but not the Intent itself)
		stackBuilder.addParentStack(MainActivity.class);
		// Adds the Intent that starts the Activity to the top of the stack
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent =
			stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
	
		mBuilder.setContentIntent(resultPendingIntent); 
		*/
		 
		mBuilder.setAutoCancel(false);
		mBuilder.setLights(Color.RED, 1000, 1000);
	
		long[] pattern = {500,500,500,500,500,500,500,500,500};
	
		mBuilder.setVibrate(pattern);
	
		Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		mBuilder.setSound(alarmSound);

		mNM = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		// mId allows you to update the notification later on.
		setKey(key);
	}
	
	public void post(){
		mNM.notify(getKey(), mBuilder.build());
	}

	/**
	 * @return the key
	 */
	public int getKey() {
		return key;
	}

	/**
	 * @param key the key to set
	 */
	public void setKey(int key) {
		this.key = key;
	}
	
	/**
	 * @return the NotificationManager
	 */
	public NotificationManager getNM() {
		return mNM;
	}
	
	/**
	 * @return the NotificationCompat.Builder
	 */
	public NotificationCompat.Builder getBuilder() {
		return mBuilder;
	}

}
