package com.CSE110.Team25.placeitapp;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;

import java.util.Calendar;
import java.util.Map;

/**
 * AddNewPlaceItFragment
 * The fragment that handles the "add new place-it" screen
 */
public class AddNewPlaceItFragment extends Fragment {
    private String title;
    private String desc;
    private double latitude;
    private double longitude;
    private String create_time;
    private boolean due_flag;
    private String rep_date;
    private String cat1 = "none";
    private String cat2 = "none";
    private String cat3 = "none";
    private AddNewPlaceItListener mCallback;

    /**
     * Listener interface for use by the main activity to provide callback functions
     */
    public interface AddNewPlaceItListener{
        public void newPlaceItAdded(PlaceIt placeIt);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.new_placeit, container, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (AddNewPlaceItListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement AddNewPlaceItListener");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        // Process arbitary data
        Bundle args = getArguments();
        this.latitude = args.getDouble("latitude");
        this.longitude = args.getDouble("longitude");
        this.create_time = String.valueOf(Calendar.HOUR_OF_DAY);
        this.due_flag = true;

        // Setup the display
        CheckBox rep_box = (CheckBox)getView().findViewById(R.id.add_repeat);
        Button cancel = (Button)getView().findViewById(R.id.add_cancel);
        Button add = (Button)getView().findViewById(R.id.add_confirm);
        Spinner cat1Spinner = (Spinner)getView().findViewById(R.id.add_cat1);
        Spinner cat2Spinner = (Spinner)getView().findViewById(R.id.add_cat2);
        Spinner cat3Spinner = (Spinner)getView().findViewById(R.id.add_cat3);
        rep_box.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    RadioGroup rd_days = (RadioGroup) getView().findViewById(R.id.add_repday);
                    rd_days.setVisibility(View.VISIBLE);
                } else {
                    RadioGroup rd_days = (RadioGroup) getView().findViewById(R.id.add_repday);
                    rd_days.setVisibility(View.INVISIBLE);
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText nTitle = (EditText)getView().findViewById(R.id.add_title);
                EditText nDesc = (EditText)getView().findViewById(R.id.add_desc);
                InputMethodManager mgr = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(nTitle.getWindowToken(), 0);
                mgr.hideSoftInputFromWindow(nDesc.getWindowToken(), 0);
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateData()) {
                    submitNewPlaceIt();
                } else {
                    Toast.makeText(getActivity(), "Missing argument. Try Again.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.category_list, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cat1Spinner.setAdapter(adapter);
        cat2Spinner.setAdapter(adapter);
        cat3Spinner.setAdapter(adapter);

        cat1Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                cat1 = (String)adapterView.getItemAtPosition(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                cat1 = "None";
            }
        });

        cat2Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                cat2 = (String)adapterView.getItemAtPosition(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                cat2 = "None";
            }
        });

        cat3Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                cat3 = (String)adapterView.getItemAtPosition(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                cat3 = "None";
            }
        });
    }

    /**
     * Validate the data input from the user and prevent addition of the place-it
     * if data is incomplete
     * Pre: informal - Data has been inputted
     * @return if data is complete, return true
     */
    public boolean validateData(){
        EditText nTitle = (EditText)getView().findViewById(R.id.add_title);
        EditText nDesc = (EditText)getView().findViewById(R.id.add_desc);
        CheckBox rep_box = (CheckBox)getView().findViewById(R.id.add_repeat);
        RadioGroup n_rdays = (RadioGroup)getView().findViewById(R.id.add_repday);
        String title;
        String desc;
        int status;

        // Close keyboard first
        InputMethodManager mgr = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(nTitle.getWindowToken(), 0);
        mgr.hideSoftInputFromWindow(nDesc.getWindowToken(), 0);

        // Check if title and description are empty
        if(nTitle.getText() == null || nDesc.getText() == null){
            Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();
            return false;
        }else{
            if(nTitle.getText().toString().isEmpty()){
                if(nDesc.getText().toString().isEmpty()){
                    return false;
                }else{
                    desc = nDesc.getText().toString();
                    title = desc.substring(0, desc.length()/2);
                }
            }else{
                title = nTitle.getText().toString();
                if(nDesc.getText().toString().isEmpty()){
                    desc = "";
                }else{
                    desc = nDesc.getText().toString();
                }
            }
        }
        
        // Check RadioButton Status
        if(rep_box.isChecked()){
            status = n_rdays.getCheckedRadioButtonId();
        }else{
            status = -1;
        }
        fillInData(title, desc, status);
        return true;
    }

    /**
     * Set all the data the user entered into the local variables and
     * prep for insertion into the database
     * Pre: All data except the title, description and repeat days have been set
     * Post: All data will be set
     * @param nTitle - Title of the place-it
     * @param nDesc - Description of the place-it
     * @param n_rdays - Day the place it will be reposted automatically
     */
    public void fillInData(String nTitle, String nDesc, int n_rdays){
        // Get title
        this.title = nTitle;
        // Get description
        this.desc = nDesc;
        // Get repeat days
        switch (n_rdays){
            case R.id.add_sun: rep_date = "sunday"; break;
            case R.id.add_mon: rep_date = "monday"; break;
            case R.id.add_tue: rep_date = "tuesday"; break;
            case R.id.add_wed: rep_date = "wednesday"; break;
            case R.id.add_thr: rep_date = "thursday"; break;
            case R.id.add_fri: rep_date = "friday"; break;
            case R.id.add_sat: rep_date = "saturday"; break;
            default: rep_date = "none";
        }
    }

    /**
     * Create the new place-it and call the callbacks to mainactivity to store the place-it
     * into the sqlite database and GAE
     * Pre: All local data have been set
     */
    public void submitNewPlaceIt(){
        PlaceIt placeIt = new PlaceIt(title, desc, latitude, longitude, create_time
                                        , due_flag, rep_date, cat1, cat2, cat3, true);
        mCallback.newPlaceItAdded(placeIt);
    }

}
