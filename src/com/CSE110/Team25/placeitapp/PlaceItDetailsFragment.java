package com.CSE110.Team25.placeitapp;

import android.app.Activity;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.maps.model.LatLng;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by apo11o on 2/22/14.
 * Allow specific information of place-it to show on screen. The fragment will 
 * show information of place-its title, description, location, and repost or delete button
 */
public class PlaceItDetailsFragment extends Fragment {
    PlaceitHandler piHandler;
    PlaceIt thePi;
    PlaceItDetailsFragmentListener mCallback;

    /**
     * Interface for the Place-it listener
     */
    public interface PlaceItDetailsFragmentListener{
        public void placeItDeleted(LatLng loc);
        public void placeItRestoredFromDetail(int id);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.placeit_details, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        Bundle pkg = getArguments();
        piHandler = PlaceitHandler.getHelper(getActivity());
        double lat = pkg.getDouble("latitude");
        double lon = pkg.getDouble("longitude");
        thePi = piHandler.getPlaceItAt(lat, lon);
        updateInfo();
    }

    /**
     * Get the information of place-it at particular location on the map and show the detailed
     * information stored in the place-it.
     */
    private void updateInfo(){
    	//get all the information of the place-it to be viewed
        TextView title = (TextView)getView().findViewById(R.id.det_title);
        TextView desc = (TextView)getView().findViewById(R.id.det_desc);
        TextView rep_day = (TextView)getView().findViewById(R.id.det_repday);
        TextView triggered = (TextView)getView().findViewById(R.id.det_triggered_flag);
        TextView location = (TextView)getView().findViewById(R.id.det_at);
        TextView cat1 = (TextView)getView().findViewById(R.id.det_cat1_box);
        TextView cat2 = (TextView)getView().findViewById(R.id.det_cat2_box);
        TextView cat3 = (TextView)getView().findViewById(R.id.det_cat3_box);
        Button del = (Button)getView().findViewById(R.id.det_del);
        Button rep = (Button)getView().findViewById(R.id.det_repost);
        title.setText(thePi.getTitle());
        desc.setText(thePi.getDesc());
        rep_day.setText(thePi.getRep_date());
        triggered.setText(Boolean.toString(!thePi.isDue_flag()));
        
        //get the location of the place-it
        try{
            Geocoder geocoder = new Geocoder(getActivity(), Locale.US);
            List<Address> addresses = geocoder.getFromLocation(thePi.getLatitude(), thePi.getLongitude(), 1);
            location.setText(addresses.get(0).getAddressLine(0));
        }catch (IOException e){
            location.setText(thePi.getLatitude() + ", " + thePi.getLongitude());
        }
        cat1.setText(thePi.getCat1());
        cat2.setText(thePi.getCat2());
        cat3.setText(thePi.getCat3());
        
        //allow buttons to function when it is clicked
        del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deletePlaceIt();
            }
        });
        rep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                placeItRestoredFromDetail();
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (PlaceItDetailsFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement PlaceItDetailsFragmentListener");
        }
    }

    /**
     * Deletes the place-it when the delete button is clicked
     */
    private void deletePlaceIt(){
        mCallback.placeItDeleted(new LatLng(thePi.getLatitude(), thePi.getLongitude()));
    }

    /**
     * Restore the place-it when the restore button is clicked
     */
    private void placeItRestoredFromDetail(){
        mCallback.placeItRestoredFromDetail(thePi.getId());
    }
}
