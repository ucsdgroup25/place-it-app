package com.CSE110.Team25.placeitapp;

import android.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

/**
 * Created by apo11o on 2/27/14.
 * Class for the tab to work along with MainActivity. 
 * When the user touch on the map when the application is opened, 
 * it will allow user to create or delete existing place-its.
 */
public class TabListener<T extends Fragment> implements ActionBar.TabListener{
    private Fragment mFragment;
    private final FragmentActivity mActivity;
    private final String mTag;
    private final Class<T>  mClass;


    /**
     * 
     * Constructor
     */
    public TabListener(FragmentActivity activity, String tag, Class<T> clz) {
        mActivity = activity;
        mTag = tag;
        mClass = clz;
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, android.app.FragmentTransaction fragmentTransaction) {
        // Check if the fragment is already initialized
        if (mFragment == null) {
            // If not, instantiate and add it to the activity
            mFragment = Fragment.instantiate(mActivity, mClass.getName());
            mActivity.getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, mFragment, mTag).commit();
        } else {
            // If it exists, simply attach it in order to show it
            mActivity.getSupportFragmentManager().beginTransaction().attach(mFragment).commit();
        }
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, android.app.FragmentTransaction fragmentTransaction) {
        mActivity.getSupportFragmentManager().beginTransaction().detach(mFragment).commit();
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, android.app.FragmentTransaction fragmentTransaction) {

    }
}
