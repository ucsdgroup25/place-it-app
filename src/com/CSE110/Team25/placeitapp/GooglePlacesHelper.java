package com.CSE110.Team25.placeitapp;

import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * GooglePlacesHelper
 * Contains all the necessary methods needed to query and parse the JSON objects
 * hosted on the Google Places API
 */
public class GooglePlacesHelper {
    private static final String API_KEY = "AIzaSyAoCQgOiTxEo2Hn9L9i3XUAu37R0bCP3jc";

    private static final String PLACES_SEARCH_URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?";
    private static final double radius = 1000;

    /**
     * Builds up the url to query Google Places API
     * Pre: Internet is available. Target location is known. Categorical place-its exists
     * Post: The built string is a valid url
     * @param lat - Target latitude
     * @param lon - Target longitude
     * @param catList - List of categories used in place-its
     * @return - The final query string
     */
    public String buildUrl(double lat, double lon, String catList){
        StringBuilder url = new StringBuilder(PLACES_SEARCH_URL);

        url.append("location=");
        url.append(Double.toString(lat));
        url.append(",");
        url.append(Double.toString(lon));
        url.append("&radius="+radius);
        url.append("&sensor=true");
        url.append("&types=" + catList.toLowerCase().substring(0, catList.length()-1).replace(' ', '_'));
        url.append("&key=");
        url.append(API_KEY);

        return url.toString();
    }

    /**
     * Constructs an array of Place-its based on the JSON data we received
     * @param lat - Target latitude
     * @param lon - Target longitude
     * @param catList - List of categories used in place-its
     * @return Array of Place-its on the server database
     */
    public ArrayList<PlaceIt> findPlaces(double lat, double lon, String catList){
        String url = buildUrl(lat, lon, catList);

        try{
            String json = grabJSONData(url);
            Log.v("findPlaces", "Places JSON: " + json);
            JSONObject object = new JSONObject(json);
            JSONArray array = object.getJSONArray("results");

            ArrayList<PlaceIt> arrayList = new ArrayList<PlaceIt>();
            PlaceIt piCur;
            for (int i = 0; i < array.length(); i++) {
                piCur = jsonToPlaceIt((JSONObject)array.get(i));
                if(piCur != null){
                    arrayList.add(piCur);
                }
            }
            return arrayList;
        }catch(Exception e){
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Grabs the JSON package from the server
     * Pre: Internet is available.
     * Post: return != null
     * @param url - The Google Places API query string
     * @return the JSON data string
     */
    private String grabJSONData(String url) {
        StringBuilder content = new StringBuilder();

        try{
            URL theUrl = new URL(url);
            URLConnection urlConnection = theUrl.openConnection();
            BufferedReader bufRead = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String temp;
            while((temp = bufRead.readLine()) != null){
                content.append(temp+ '\n');
            }
            bufRead.close();
        }catch(Exception e){
            e.printStackTrace();
        }

        Log.v("grabJSONData", content.toString());
        return content.toString();
    }

    /**
     * Converts a JSON blob into a valid Place-it
     * Pre: JSON blob is not null
     * Post: Valid Place-it is created
     * @param blob - A single JSON object
     * @return
     */
    public PlaceIt jsonToPlaceIt(JSONObject blob){
        try {
            PlaceIt pi = new PlaceIt();
            JSONObject geo = (JSONObject)blob.get("geometry");
            JSONObject loc = (JSONObject)geo.get("location");
            JSONArray types = blob.getJSONArray("types");
            pi.setTitle(blob.getString("name"));
            pi.setDesc(blob.getString("vicinity"));
            pi.setLatitude((Double) loc.get("lat"));
            pi.setLongitude((Double) loc.get("lng"));
            pi.setCreate_time("");
            pi.setDue_flag(true);
            pi.setRep_date("none");
            try{
                pi.setCat1(wordCapitalize(types.getString(0)));
            }catch(JSONException e){
                pi.setCat1("None");
            }
            try{
                pi.setCat2(wordCapitalize(types.getString(1)));
            }catch(JSONException e){
                pi.setCat2("None");
            }
            try{
                pi.setCat3(wordCapitalize(types.getString(3)));
            }catch(JSONException e){
                pi.setCat3("None");
            }
            pi.setIsPlaceit(false);
            return pi;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Transforms the category query string used by the Google Place API to a more
     * human readable format
     * Pre: input is not empty nor null
     * @param input - string to be converted
     * @return human readable format of the input string
     */
    private String wordCapitalize(String input){
        String[] temp = input.split("_");
        StringBuilder res = new StringBuilder();

        for(String s: temp){
            res.append(Character.toUpperCase(s.charAt(0)) + s.substring(1, s.length()) + " ");
        }

        return res.toString().trim();
    }
}
