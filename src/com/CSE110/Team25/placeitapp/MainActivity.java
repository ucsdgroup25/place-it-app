package com.CSE110.Team25.placeitapp;

import android.app.*;
import android.content.*;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.MenuItemCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.model.LatLng;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * MainActivity
 * Using the Mediator pattern. Handles all the calls and callbacks to different components
 * of the UI fragments and database querying
 */
public class MainActivity extends FragmentActivity
        implements PlaceItListFragment.OnPlaceItSelectedListener,
        MyMapFragment.PlaceItAddedListener, PlaceItDetailsFragment.PlaceItDetailsFragmentListener,
        AddNewPlaceItFragment.AddNewPlaceItListener, GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener, LocationListener
{
    private static final String SELECTED_TAB_STATUS = "selected_tab_status";
    private static final String PLACEIT_URI = "http://cse110team25mycity.appspot.com/placeit";
    PlaceitHandler piHandler = PlaceitHandler.getHelper(this);
    private ProgressDialog pDialog;
    
    /*
     * Define a request code to send to Google Play services
     * This code is returned in Activity.onActivityResult
     * 
     * Constants used for location crap
     */
    private final static int
    CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    // Milliseconds per second
    private static final int MILLISECONDS_PER_SECOND = 1000;
    // Update frequency in seconds
    public static final int UPDATE_INTERVAL_IN_SECONDS = 5;
    // Update frequency in milliseconds
    private static final long UPDATE_INTERVAL =
            MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS;
    // The fastest update frequency, in seconds
    private static final int FASTEST_INTERVAL_IN_SECONDS = 1;
    // A fast frequency ceiling in milliseconds
    private static final long FASTEST_INTERVAL =
            MILLISECONDS_PER_SECOND * FASTEST_INTERVAL_IN_SECONDS;
    
    // Define an object that holds accuracy and frequency parameters
    LocationRequest mLocationRequest;
    
    // Stores the current instantiation of the location client in this object
    private LocationClient mLocationClient;
    
    private static Context context;
    
    // Handle to SharedPreferences for this app
    SharedPreferences mPrefs;

    // Handle to a SharedPreferences editor
    SharedPreferences.Editor mEditor;
    
    private BroadcastReceiver mReceiver;
    
    public static Context getAppContext() {
        return MainActivity.context;
    }
    
    public LocationClient getLocationClient(){
    	return mLocationClient;
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainActivity.context = getApplicationContext();
        setContentView(R.layout.main);

        final ActionBar actionBar = getActionBar();
        if(actionBar!=null){
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
            actionBar.addTab(actionBar.newTab()
                    .setText(R.string.map_tab)
                    .setTabListener(new TabListener<MyMapFragment>(
                            this, "my_map", MyMapFragment.class)));
            actionBar.addTab(actionBar.newTab()
                    .setText(R.string.list_tab)
                    .setTabListener(new TabListener<PlaceItListFragment>(
                            this, "pi_list", PlaceItListFragment.class)));
            //actionBar.setDisplayHomeAsUpEnabled(true);
            getActionBar().show();
        }
        piHandler.open();
       
        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create();
        // Use high accuracy
        mLocationRequest.setPriority(
                LocationRequest.PRIORITY_HIGH_ACCURACY);
        // Set the update interval to 5 seconds
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        // Set the fastest update interval to 1 second
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        
        /*
         * Create a new location client, using the enclosing class to
         * handle callbacks.
         */
        mLocationClient = new LocationClient(this, this, this);

    }
    
    /**
	 * Binding the geolocation monitoring service to the MainActivity
	 * Followed by onResume() if the activity comes to the foreground,
	 * or onStop() if it becomes hidden.
	 */
    @Override
    public void onStart() {
    	super.onStart();
    	//bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE);
        mLocationClient.connect();
    }

    /**
	 * Unbinds the service when the app stops
	 * Followed by either onRestart() if this activity is coming 
	 * back to interact with the user, or onDestroy() if this 
	 * activity is going away.
	 */
	@Override
	public void onStop() {
		super.onStop();
		
		/*if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }*/
        // If the client is connected
        if (mLocationClient.isConnected()) {
            stopPeriodicUpdates();
        }

        // After disconnect() is called, the client is considered "dead".
        mLocationClient.disconnect();
	}

    /**
     * Checks if any scheduled place-its are restored and handles the broadcast intent.
     */
	@Override
	public void onResume(){
		super.onResume();
		 // We listen for a broadcast Intent with
        // action = com.CSE110.Team25.placeitapp.ACTION_REDRAW_PLACEIT;
        piHandler.restoreScheduledPlaceIts();
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.CSE110.Team25.placeitapp.ACTION_REDRAW_PLACEIT");

       
        // Init the receiver
        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {                   
                // Reload when we receive the broadcast
                MainActivity.this.placeItModified();
            }
        };
        
        //new UpdatePlaceitTask().execute(MainActivity.PLACEIT_URI);
       
        // Register the receiver
        this.registerReceiver(mReceiver, filter);
	}

    /**
     * Sends any updated place-its to GAE and un-registers the receiver
     */
	@Override
	public void onPause(){
		super.onPause();
        for(PlaceIt pi: piHandler.getAllPlaceIts()){
            new SendData().execute(pi);
        }
		//unregister our receiver
		this.unregisterReceiver(this.mReceiver);
	}
	
	/*
    /**
     * Defines callbacks for service binding, passed to bindService()
   
    private ServiceConnection mConnection = new ServiceConnection() {*/

    	/*
        @Override
        public void onServiceConnected(ComponentName className,
                IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            LocalBinder binder = (LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };*/

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // Restore last selected tab
        if (savedInstanceState.containsKey(SELECTED_TAB_STATUS)) {
            if(getActionBar()!=null)
                getActionBar().setSelectedNavigationItem(savedInstanceState.getInt(SELECTED_TAB_STATUS));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save current selected tab
        if(getActionBar()!= null)
            outState.putInt(SELECTED_TAB_STATUS, getActionBar().getSelectedNavigationIndex());
    }

    /**
     * Callback method for PlaceItListFragment that displays details fo a selected Place-it
     * Pre: placeIt != null
     * Post: PlaceItDetailsFragment is called
     * @param placeIt - Place-it that was selected
     */
    @Override
    public void onPlaceItSelected(PlaceIt placeIt) {
        PlaceItDetailsFragment newFragment = new PlaceItDetailsFragment();
        Bundle args = new Bundle();
        args.putDouble("latitude", placeIt.getLatitude());
        args.putDouble("longitude", placeIt.getLongitude());
        newFragment.setArguments(args);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, newFragment).addToBackStack(null)
                .commit();
    }

    /**
     * Callback function for MyMapFragment
     * Pre: User clicked a point on the map
     * Post: AddNewPlaceItFragment is displayed
     * @param loc - the location on the map where the user clicked
     */
    @Override
    public void addPlaceItFromMap(LatLng loc) {
        Toast.makeText(this, "Map Clicked at: " + loc.toString(), Toast.LENGTH_SHORT).show();
        AddNewPlaceItFragment newFragment = new AddNewPlaceItFragment();
        Bundle args = new Bundle();
        args.putDouble("latitude", loc.latitude);
        args.putDouble("longitude", loc.longitude);
        newFragment.setArguments(args);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, newFragment, "add_placeit")
                .addToBackStack("add_placeit")
                .commit();
    }

    /**
     * Callback method for MyMapFragment
     * Called when marker is clicked
     * Pre: Marker exists and is tied to a place-it
     * Post: PlaceItDetailsFragment is displayed
     * @param loc - location of the marker
     * @param mID - id of the marker
     */
    @Override
    public void placeItDetails(LatLng loc, String mID) {
        Toast.makeText(this, "More details?", Toast.LENGTH_SHORT).show();
        // Create fragment and give it an argument for the selected article
        PlaceItDetailsFragment newFragment = new PlaceItDetailsFragment();
        Bundle args = new Bundle();
        args.putDouble("latitude", loc.latitude);
        args.putDouble("longitude", loc.longitude);
        newFragment.setArguments(args);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, newFragment, "pi_detail")
                .addToBackStack("pi_detail")
                .commit();
    }

    /**
     * Updates the markers on the Map based on the updated database
     * Pre: MyMapFragment has already been created
     * Post: Markers updated on the map
     */
    public void placeItModified() {
        MyMapFragment mMap = (MyMapFragment)getSupportFragmentManager().findFragmentByTag("my_map");
        mMap.clearAllPlaceitsFromMap();
        mMap.restorePlaceits();
    }

    /**
     * Callback for PlaceItDetailsFragment
     * Deletes place-it from the local sqlite database and GAE and update the markers on the map
     * Pre: Place-it exists in the database
     * Post: Place-it is deleted from the database
     * @param loc - location of the place-it that should be deleted
     */
    @Override
    public void placeItDeleted(LatLng loc) {
        PlaceIt temp = piHandler.getPlaceItAt(loc.latitude, loc.longitude);
        piHandler.deletePlaceItAt(loc.latitude, loc.longitude);
        new RequestDelete().execute(temp);
        placeItModified();
        getSupportFragmentManager().popBackStack();
    }

    /**
     * Callback for PlaceItDetailsFragment
     * Reposts the place-it from the details screen
     * Pre: Place-it has already been triggered
     * Post: Place-it is ready to be triggered
     * @param id - the id fo the Place-it in the database
     */
    @Override
    public void placeItRestoredFromDetail(int id) {
        piHandler.updatePlaceItInDatabase(id, MySQLHelper.KEY_DUE_FLAG, "true");
        placeItModified();
        getSupportFragmentManager().popBackStack();
    }

    /**
     * Callback for AddNewPlaceItFragment
     * Adds the new place-it to the sqlite database and GAE and updates the map
     * Pre: place-it != null
     * Post: Place-it added to both databases
     * @param placeIt - the new place-it to be added
     */
    @Override
    public void newPlaceItAdded(PlaceIt placeIt) {
        piHandler.addPlaceIt(placeIt);
        //new LoadPlaces().execute(new LatLng(placeIt.getLatitude(), placeIt.getLongitude()));
        new SendData().execute(placeIt);
        placeItModified();
        getSupportFragmentManager().popBackStack();
    }

    /**
     * AsyncTask to load the places from Google Places API for categorical place-its
     * Pre: Place-it with categories exist in the database
     * Post: List of places with the same categories as existing place-its is added
     * to the local database
     */
    class LoadPlaces extends AsyncTask<LatLng, Void, ArrayList<PlaceIt>>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Finishing up transaction. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected ArrayList<PlaceIt> doInBackground(LatLng... latLngs) {
            GooglePlacesHelper gpHelper = new GooglePlacesHelper();
            Log.v("Places API", piHandler.getAllUsedCategoriesAsString());
            if(piHandler.getAllUsedCategoriesAsString().isEmpty()){
                return new ArrayList<PlaceIt>();
            }else{
                return gpHelper.findPlaces(latLngs[0].latitude, latLngs[0].longitude,
                    piHandler.getAllUsedCategoriesAsString());
            }
        }

        @Override
        protected void onPostExecute(ArrayList<PlaceIt> arrayList) {
            if(arrayList != null){
                //Toast.makeText(getAppContext(), arrayList.toString(), Toast.LENGTH_SHORT).show();
                for(PlaceIt pi: arrayList){
                    if(!piHandler.piIsInDatabase(pi.getLatitude(), pi.getLongitude())){
                        piHandler.addPlaceIt(pi);
                    }
                }
            }
            pDialog.hide();
        }
    }

    /**
     * AsyncTask to send place-it data to GAE
     * Pre: Place-it had already been added to the local database
     * Post: The Place-it shows up in GAE
     */
    private class SendData extends AsyncTask<PlaceIt, Void, Void> {
        @Override
        protected Void doInBackground(PlaceIt... placeit) {
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(MainActivity.PLACEIT_URI);
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(7);

            try{
                nameValuePairs.add(new BasicNameValuePair("name",
                        placeit[0].getTitle()));
                nameValuePairs.add(new BasicNameValuePair("description",
                        placeit[0].getDesc()));
                nameValuePairs.add(new BasicNameValuePair("latitude",
                        Double.toString(placeit[0].getLatitude())));
                nameValuePairs.add(new BasicNameValuePair("longitude",
                        Double.toString(placeit[0].getLongitude())));
                nameValuePairs.add(new BasicNameValuePair("create_time",
                        placeit[0].getCreate_time()));
                nameValuePairs.add(new BasicNameValuePair("due_flag",
                        Boolean.toString(placeit[0].isDue_flag())));
                nameValuePairs.add(new BasicNameValuePair("rep_date",
                        placeit[0].getRep_date()));
                nameValuePairs.add(new BasicNameValuePair("cat1",
                        placeit[0].getCat1()));
                nameValuePairs.add(new BasicNameValuePair("cat2",
                        placeit[0].getCat2()));
                nameValuePairs.add(new BasicNameValuePair("cat3",
                        placeit[0].getCat3()));
                nameValuePairs.add(new BasicNameValuePair("action",
                        "put"));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                HttpResponse response = client.execute(post);
                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                String line = "";
                while ((line = rd.readLine()) != null) {
                    //Log.d(TAG, line);
                }
            }catch (IOException e){}
            return null;

        }
    }
    
    private class UpdatePlaceitTask extends AsyncTask<Void, Void, ArrayList<PlaceIt>> {
    	
    	@Override
		protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Updating placeits. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
		}

		@Override
    	protected ArrayList<PlaceIt> doInBackground(Void... Void) {
    	
    		HttpClient client = new DefaultHttpClient();
    		HttpGet request = new HttpGet(MainActivity.PLACEIT_URI);
    		ArrayList<PlaceIt> list = new ArrayList<PlaceIt>();
    		try {
    			HttpResponse response = client.execute(request);
    			HttpEntity entity = response.getEntity();
    			String data = EntityUtils.toString(entity);
    			JSONObject myjson;
    			try {
    				myjson = new JSONObject(data);
    				JSONArray array = myjson.getJSONArray("data");
    				Log.v("AsyncBlah1", array.toString());
    				for (int i = 0; i < array.length(); i++) {
    					PlaceIt temp = jsonToPlaceIt((JSONObject)array.get(i));
    					if(temp != null){
    						list.add(temp);
    					}
    				}						
    			} catch (JSONException e) {	
    				Log.v("AsyncBlahException", "Bummer");
    			}		
    			Log.v("AsyncBlahhhhhh", list.toString());
    			return list;
    		} catch (ClientProtocolException e) {
    		} catch (IOException e) {
    		}
    		return null;
    	}
    	
    	/**
         * Converts a JSON blob into a valid Place-it
         * Pre: JSON blob is not null
         * Post: Valid Place-it is created
         * @param blob - A single JSON object
         * @return
         */
        public PlaceIt jsonToPlaceIt(JSONObject blob){
            try {
                PlaceIt pi = new PlaceIt();
                pi.setTitle(blob.getString("name"));
                pi.setDesc(blob.getString("description"));
                pi.setLatitude(Double.parseDouble(blob.getString("lat")));
                pi.setLongitude(Double.parseDouble(blob.getString("long")));
                pi.setCreate_time(blob.getString("create_time"));
                pi.setDue_flag(Boolean.parseBoolean(blob.getString("due_flag")));
                pi.setRep_date(blob.getString("rep_date"));
                pi.setCat1(blob.getString("cat1"));
                pi.setCat2(blob.getString("cat2"));
                pi.setCat3(blob.getString("cat3"));
                pi.setIsPlaceit(true);
                Log.v("jjsonService", pi.toString());
                return pi;
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

    	@Override
    	protected void onPostExecute(ArrayList<PlaceIt> remote) {
    		Double corner = 0.000000000000001;

    		ArrayList<PlaceIt> local = piHandler.getAllPlaceIts();
    		Log.v("AsyncBlah2", remote.toString());
    		if(remote != null){
                //Remove place-its in local (sync local data to server)
                for(PlaceIt piloc: local){
                    boolean flag = true;
                    for(PlaceIt pilocal: remote){
                        if(pilocal.equals(piloc)){
                            flag = false;
                        }
                        if(flag){ piHandler.deletePlaceItAt(piloc.getLatitude(), piloc.getLongitude());
                        }
                    }
                }
                // Add missing place-its in local
    			for(PlaceIt pi: remote){
    				if(piHandler.getPlaceItAt(pi.getLatitude(), pi.getLongitude()) == null){
    					piHandler.addPlaceIt(pi);
    				}
    			}
    		}
            placeItModified();
    		pDialog.hide();
    	}
    }

    /**
     * Deletes a place-it from the GAE
     * Pre: Place-it has been deleted from the local database
     * Post: Place-it also deleted from GAE
     */
    private class RequestDelete extends AsyncTask<PlaceIt, Void, Void> {
        @Override
        protected Void doInBackground(PlaceIt... placeit) {
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(MainActivity.PLACEIT_URI);
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(7);

            try{
                nameValuePairs.add(new BasicNameValuePair("id",
                        Double.toString(placeit[0].getLatitude()) +
                                Double.toString(placeit[0].getLongitude())));

                nameValuePairs.add(new BasicNameValuePair("action",
                        "delete"));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = client.execute(post);
            }
            catch (IOException e){}
            return null;

        }
    }

    /**
     * Process the cases when the intent calls up the app
     * Pre: intent != null
     * Post: The intent action is performed
     * @param intent - the intent with the info needed to perform
     *               required action
     */
    protected void onNewIntent(Intent intent){
    	String action_name = intent.getAction();
    	//Toast.makeText(MainActivity.this, action_name, Toast.LENGTH_SHORT).show();
    	//Bundle extra = intent.getExtras();
    	int key = intent.getIntExtra("myID", 0);
    	//Toast.makeText(MainActivity.this, Integer.toString(key), Toast.LENGTH_SHORT).show();
		
		NotificationManager mNM = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		
		//debug string
		//Toast.makeText(this, "Inside onNewIntent", Toast.LENGTH_SHORT).show(); 
		for (PlaceIt myPlaceIt: piHandler.getAllPlaceIts()) {
			//debug string
			//Toast.makeText(this, "Inside for loop, key = " + Integer.toString(key), Toast.LENGTH_SHORT).show();
			if(myPlaceIt == null){
				Toast.makeText(this, "Returned null in notification", Toast.LENGTH_SHORT).show();
				break;
			}
			else{
				if(myPlaceIt.getId() == key){
					
					if (action_name.equals("repost")) {
						//debug string
						//Toast.makeText(this, "Inside Repost", Toast.LENGTH_SHORT).show();
						piHandler.updatePlaceItInDatabase(myPlaceIt.getId(), MySQLHelper.KEY_DUE_FLAG, "true");
						this.placeItModified();
						mNM.cancel(key);
			        }
					else if (action_name.equals("discard")) {
						//debug string
						//Toast.makeText(this, "Inside Discard", Toast.LENGTH_SHORT).show();
                        if(piHandler.getPlaceItAt(myPlaceIt.getLatitude(),myPlaceIt.getLongitude()).isPlaceit())
						    this.placeItDeleted(new LatLng(myPlaceIt.getLatitude(),myPlaceIt.getLongitude()));
						mNM.cancel(key);
					}
					//Currently this should never be called, pressing 
					else if (action_name.equals("notification")) {
						//debug string
			        	//notificationNote(myPlaceIt);'
						Toast.makeText(this, "Inside Note", Toast.LENGTH_SHORT).show();
			        }
			        
					//	Toast.makeText(MainActivity.this, "Reloading Intent!", Toast.LENGTH_SHORT).show();
					Intent reset = new Intent(this, MainActivity.class);
					startActivity(reset);
					//finish();
					//startActivity(getIntent());
					//Toast.makeText(MainActivity.this, "Done Reloading Intent!", Toast.LENGTH_SHORT).show();
					
					break;
				}
			}
		}
    }

    /**
     * ActionBar action callbacks setup
     * Pre: ActionBar has been initialized
     * Post: The ActionBar items have been initialized
     * @param menu - interface for managing menu items
     * @return - If the given operation was performed correctly, return true. false otherwise.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                moveCamera(s);
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }


    /**
     * Callbacks for the actionbar items
     * Pre: Each actionbar item has been initialized
     * Post: Option performed
     * @param item - menu item
     * @return - if the operation was completed successfully, return true. false otherwise
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_sync_gae:
                new UpdatePlaceitTask().execute(); return true;
            case R.id.action_pull_places:
                new LoadPlaces().execute(new LatLng(mLocationClient.getLastLocation().getLatitude(),
                        mLocationClient.getLastLocation().getLongitude())); return true;
            case R.id.stop_service: return true;
            case R.id.start_service: return true;
            default: return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Moves the map location based off the street or keyword location
     * Pre: Entered string is not empty
     * Post: Map location will be changed if the street was valid
     * @param street - keyword to be searched
     */
    private void moveCamera(String street){
        MyMapFragment mMap = (MyMapFragment)getSupportFragmentManager().findFragmentByTag("my_map");
        mMap.moveCameraTo(street);
    }
    
    /**
     * From all the place-it's in the 1.5 mile radius, will notify those
     * that are still active to the user including categorical place-its.
     * When the place-it is notified, the expiration flag will automatically
     * be set to true in the database.
     * Pre: current location is available
     * Post: all place-its that fit the criteria is triggered and notified
     * to the user
     */
    private void findPlaceIts(Location location) {
		double currentLat = location.getLatitude();
		double currentLng = location.getLongitude();

        for(PlaceIt myPlaceIt: piHandler.getAllPlaceItsAround(currentLat, currentLng)){
			if(myPlaceIt == null){
				Toast.makeText(this, "Returned a null in findPlaceIts", Toast.LENGTH_SHORT).show();
				break;
			}else{
				if(myPlaceIt.isDue_flag()){
                    for(PlaceIt pi: piHandler.getPlaceItsWithCategories(myPlaceIt.getCategories())){
                        MyNotificationHelper temp = new MyNotificationHelper(this, pi.getTitle(),
                                pi.getDesc(), pi.getTitle(), pi.getDesc(),
                                "", R.drawable.ic_action_paste, pi.getId());
                        temp.post();
                        piHandler.updatePlaceItInDatabase(pi.getId(), MySQLHelper.KEY_DUE_FLAG, "false");
                    }

					MyNotificationHelper temp = new MyNotificationHelper(this, myPlaceIt.getTitle(),
							myPlaceIt.getDesc(), myPlaceIt.getTitle(), myPlaceIt.getDesc(),
                                        "", R.drawable.ic_action_paste, myPlaceIt.getId());
					temp.post();
                    piHandler.updatePlaceItInDatabase(myPlaceIt.getId(), MySQLHelper.KEY_DUE_FLAG, "false");
				}
			}
		}


        for(PlaceIt myPlaceIt: piHandler.getAllNonPlaceItsAround(currentLat, currentLng)){
            String mesg = myPlaceIt.getDesc();
            ArrayList<PlaceIt> buffer = piHandler.getPlaceItsWithCategories(myPlaceIt.getCategories());
            boolean flag = false;
            if(!buffer.isEmpty()){
                for(PlaceIt pi: buffer){
                    if(pi.isDue_flag()){
                        flag = true;
                        mesg += ("\n" + pi.getTitle() + "\n" + pi.getDesc() + "\n");
                        piHandler.updatePlaceItInDatabase(pi.getId(), MySQLHelper.KEY_DUE_FLAG, "false");
                    }
                }
                if(flag){
                    MyNotificationHelper temp = new MyNotificationHelper(this, myPlaceIt.getTitle(),
                            mesg, myPlaceIt.getTitle(), mesg,
                            "", R.drawable.ic_action_paste, myPlaceIt.getId());
                    temp.post();
                }
            }
        }

        Intent redraw = new Intent("com.CSE110.Team25.placeitapp.ACTION_REDRAW_PLACEIT");
        // Broadcast that Intent
        sendBroadcast(redraw);
	}
    
    /**
     *******************************************
     *******************************************
     *******************************************
     * NEW CODE FOR NEW LOCATION SERVICE THING *
     *******************************************
     *******************************************
     *******************************************
     */
    /**
     * Define a DialogFragment that displays the error dialog
     */
    public static class ErrorDialogFragment extends DialogFragment {
        // Global field to contain the error dialog
        private Dialog mDialog;
        // Default constructor. Sets the dialog field to null
        public ErrorDialogFragment() {
            super();
            mDialog = null;
        }
        // Set the dialog to display
        public void setDialog(Dialog dialog) {
            mDialog = dialog;
        }
        // Return a Dialog to the DialogFragment.
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return mDialog;
        }
    }
    
    /**
     * Handle results returned to the FragmentActivity
     * by Google Play services
     */
    @Override
    protected void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        // Decide what to do based on the original request code
        switch (requestCode) {
            case CONNECTION_FAILURE_RESOLUTION_REQUEST :
            /*
             * If the result code is Activity.RESULT_OK, try
             * to connect again
             */
                switch (resultCode) {
                    case Activity.RESULT_OK :
                    /*
                     * Try the request again
                     */
                    break;
                }
        }
    }
    
    /**
     * Called by Location Services when the request to connect the
     * client finishes successfully. At this point, you can
     * request the current location or start periodic updates
     */
    @Override
    public void onConnected(Bundle dataBundle) {
        // Display the connection status
        Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();
        
        startPeriodicUpdates();
    }
    
    /**
     * Called by Location Services if the connection to the
     * location client drops because of an error.
     */
    @Override
    public void onDisconnected() {
        // Display the connection status
        Toast.makeText(this, "Disconnected. Please re-connect.",
                Toast.LENGTH_SHORT).show();
    }
    
    /**
     * Show a dialog returned by Google Play services for the
     * connection error code
     *
     * @param errorCode An error code returned from onConnectionFailed
     */
    private void showErrorDialog(int errorCode) {

        // Get the error dialog from Google Play services
        Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
            errorCode,
            this,
            LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST);

        // If Google Play services can provide an error dialog
        if (errorDialog != null) {

            // Create a new DialogFragment in which to show the error dialog
            ErrorDialogFragment errorFragment = new ErrorDialogFragment();

            // Set the dialog in the DialogFragment
            errorFragment.setDialog(errorDialog);

            // Show the error dialog in the DialogFragment
            errorFragment.show(getSupportFragmentManager(), LocationUtils.APPTAG);
        }
    }
    
    /**
     * Called by Location Services if the attempt to
     * Location Services fails.
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        this,
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                * Thrown if Google Play services canceled the original
                * PendingIntent
                */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
            showErrorDialog(connectionResult.getErrorCode());
        }
    }
    
    
    /**
     * In response to a request to start updates, send a request
     * to Location Services
     */
    private void startPeriodicUpdates() {

        mLocationClient.requestLocationUpdates(mLocationRequest, this);
    }
    
    /**
     * In response to a request to stop updates, send a request to
     * Location Services
     */
    private void stopPeriodicUpdates() {
        mLocationClient.removeLocationUpdates(this);
    }

    /**
     * Report location updates to the UI.
     *
     * @param location The updated location.
     */
    @Override
    public void onLocationChanged(Location location) {
        // Report to the UI that the location was updated
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        //Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        findPlaceIts(location);
        
    }
}
