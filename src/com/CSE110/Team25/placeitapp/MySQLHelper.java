/**
 * CSE110: Team 25
 * MySQLHelper class
 * Extends the SQLiteOpenHelper built-in to Android to setup
 * the SQLite database we are using for the app
 */

package com.CSE110.Team25.placeitapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
/**
 * Class for MySQLite Helper that is responsible for creating database. 
 * 
 * @author Dayoun
 *
 */
public class MySQLHelper extends SQLiteOpenHelper {
	/**
	 * Give static String key for each value that will be stored inside database
	 */
    public static final String KEY_ROWID = "_id";
    public static final String KEY_TITLE = "title";
    public static final String KEY_DESC = "desc";
    public static final String KEY_LANG = "lang";
    public static final String KEY_LONG = "long";
    public static final String KEY_CREATE = "creation";
    public static final String KEY_DUE_FLAG = "due_flag";
    public static final String KEY_REPEAT_DAY = "repeat_day";
    public static final String KEY_CAT1 = "category_1";
    public static final String KEY_CAT2 = "category_2";
    public static final String KEY_CAT3 = "category_3";
    public static final String KEY_IS_PI = "pi_flag";

    public static final String DATABASE_NAME = "placeits.db";
    public static final String DATABASE_TABLE = "placeits";
    public static final int DATABASE_VERSION = 1;

    //create String containing all strings for database
    public static final String DATABASE_CREATE =
            "CREATE TABLE " + DATABASE_TABLE + " ("
                    + KEY_ROWID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + KEY_TITLE + " TEXT NOT NULL, "
                    + KEY_DESC + " TEXT NOT NULL, "
                    + KEY_LANG + " TEXT NOT NULL, "
                    + KEY_LONG + " TEXT NOT NULL, "
                    + KEY_CREATE + " TEXT NOT NULL, "
                    + KEY_DUE_FLAG + " TEXT NOT NULL, "
                    + KEY_REPEAT_DAY + " TEXT NOT NULL, "
                    + KEY_CAT1 + " TEXT NOT NULL, "
                    + KEY_CAT2 + " TEXT NOT NULL, "
                    + KEY_CAT3 + " TEXT NOT NULL, "
                    + KEY_IS_PI + " TEXT NOT NULL);";

    /**
     * Constructor
     */
    public MySQLHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(MySQLHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
        onCreate(db);
    }

}
