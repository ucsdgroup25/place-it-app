package com.CSE110.Team25.placeitapp;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.widget.Toast;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * MyMapFragment
 * Handles all map related tasks including markers and camera movement
 */
public class MyMapFragment extends SupportMapFragment {
    private GoogleMap mMap;
    private PlaceitHandler piHandler;
    PlaceItAddedListener mCallback;
    private List<Marker> mMarkers = new ArrayList<Marker>();
    SharedPreferences sharedPref;

    public interface PlaceItAddedListener{
        public void addPlaceItFromMap(LatLng loc);
        public void placeItDetails(LatLng loc, String mID);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPref = getActivity().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setUpMapIfNeeded();
        piHandler = PlaceitHandler.getHelper(getActivity());
        restorePlaceits();
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            mMap = getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                // The Map is verified. It is now safe to manipulate the map.
                mMap.getUiSettings().setZoomControlsEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
                mMap.getUiSettings().setRotateGesturesEnabled(true);
                mMap.setMyLocationEnabled(true);
                mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {
                        mCallback.addPlaceItFromMap(latLng);
                    }
                });
                mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        LatLng loc = new LatLng(marker.getPosition().latitude, marker.getPosition().longitude);
                        mCallback.placeItDetails(loc, marker.getId());
                    }
                });
                setRetainInstance(true);
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (PlaceItAddedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement PlaceItAddedListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        float latitude = sharedPref.getFloat("latitude", (float) 32);
        float longitude = sharedPref.getFloat("longitude", (float) -117);
        float zoom = sharedPref.getFloat("zoom", (float)50);
        CameraPosition.Builder cameraBuilder = new CameraPosition.Builder();
        LatLng location = new LatLng(latitude, longitude);
        cameraBuilder.target(location);
        cameraBuilder.zoom(zoom);
        CameraPosition cameraPosition = cameraBuilder.build();
        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        restorePlaceits();
    }

    @Override
    public void onPause() {
        super.onPause();
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putFloat("latitude", (float) mMap.getCameraPosition().target.latitude);
        editor.putFloat("longitude", (float) mMap.getCameraPosition().target.longitude);
        editor.putFloat("zoom", mMap.getCameraPosition().zoom);
        editor.commit();
        clearAllPlaceitsFromMap();
    }

    /**
     * Re-draws the markers on the map and changes the icon based on whether it was
     * triggered or not
     */
    public void restorePlaceits(){
        //Toast.makeText(MainActivity.this, "Restoring Place-its", Toast.LENGTH_SHORT).show();
        ArrayList<PlaceIt> allPi = piHandler.getAllPlaceIts();
        //ArrayList<PlaceIt> allPi = piHandler.getAllEntries();
        Toast.makeText(getActivity(), "Restoring (" + piHandler.getAllEntries().size() + ")...", Toast.LENGTH_SHORT).show();
        for(PlaceIt curPi: allPi){
            LatLng mLoc = new LatLng(curPi.getLatitude(), curPi.getLongitude());
            MarkerOptions temp = new MarkerOptions().position(mLoc).title(curPi.getTitle()).snippet(curPi.getDesc());
            if( curPi.isDue_flag() ){
                temp.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_place_it_map));
            }else{
                temp.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_place_it_map_gray));
            }
            Marker added = mMap.addMarker(temp);
            mMarkers.add(added);
        }
    }

    /**
     * Moves the camera location on the map to a location specified
     * by the string
     * @param street - keyword or street address
     */
    public void moveCameraTo(String street){
        try{
            Geocoder geocoder = new Geocoder(getActivity(), Locale.US);
            List<Address> addresses = geocoder.getFromLocationName(street, 1);
            CameraPosition.Builder cameraBuilder = new CameraPosition.Builder();
            if(!addresses.isEmpty()){
                LatLng location = new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude());
                cameraBuilder.target(location);
                cameraBuilder.zoom(15);
                CameraPosition cameraPosition = cameraBuilder.build();
                mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }else{
                Toast.makeText(getActivity(), "Invalid location", Toast.LENGTH_SHORT).show();
            }
        }catch (IOException e){
            Toast.makeText(getActivity(), "Invalid location", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * All place-its on the map will be deleted
     */
    public void clearAllPlaceitsFromMap(){
        for(Marker cur: mMarkers){
            cur.remove();
        }
        mMap.clear();
    }

    /**
     * All markers on the map is returned
     * @return list of all markers on the map
     */
    public List<Marker> getMarkers(){
        return mMarkers;
    }
}
